@extends('app')

@section('content')

<h1>{{ $todo->task }}</h1>
<p>Due: {{ $todo->due_date }}</p>
<p>Completed: {{ $todo->completed }}</p>
<p>Group: <a href="{{ action('GroupController@show', [$todo->group->id]) }}">{{ $todo->group->name }}</a></p>

@endsection