@extends('app')

@section('content')

<h1>Add a new To-Do</h1>

{!! Form::open(['action' => ['ToDoController@store', $groupId]]) !!}
    @include('todos.form')
{!! Form::close() !!}

@endsection