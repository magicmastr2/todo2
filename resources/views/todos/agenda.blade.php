@extends('app')

@section('content')

<div class="row">
    @include('groups._group_list')

    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ action('ToDoController@create', [$group->id]) }}" class="btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span></a>
                <h1 class="panel-title">
                    {{ $group->name }} To-Dos
                </h1>
            </div>
             <ul class="list-group">
               <?php $d=null;?>
                 @foreach($todos as $date)
                     @if($d == null || $d != $date->due_date)
                         <?php $d=$date->due_date; ?>
                         <h2>{{ $date->due_date }}</h2>
                     @endif
                    {{ $date->task }}
               <br/>
                 @endforeach

@endsection