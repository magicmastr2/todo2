@extends('app')

@section('content')

<div class="row">
    @include('groups._group_list')

    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ action('ToDoController@create', [$group->id]) }}" class="btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span></a>
                <h1 class="panel-title">
                    {{ $group->name }} To-Dos
                </h1>
            </div>
            <ul class="list-group">
                @foreach($group->toDos as $todo)
                    <li class="list-group-item">
                        {!! Form::open(['action' => ['ToDoController@toggleCompleted', $group->id, $todo->id], 'method' => 'PATCH']) !!}
                            @if($todo->completed)
                                {!! Form::hidden('completed', false) !!}
                                <button class="btn btn-default btn-xs pull-left"><span class="glyphicon glyphicon-check"></span></button>
                            @else
                                {!! Form::hidden('completed', true) !!}
                                <button class="btn btn-default btn-xs pull-left"><span class="glyphicon glyphicon-unchecked"></span></button>
                            @endif
                        {!! Form::close() !!}
                        &nbsp;{{ $todo->task }}
                        {{ Form::model($todo, [
                            'method' => 'DELETE',
                            'action' => ['ToDoController@destroy', $todo->id],
                            'class' => 'pull-right',
                        ]) }}
                            {{-- <span class="pull-right"> --}}
                                Due: {{ $todo->due_date }}, {{ $todo->time }}
                                <a href="{{ action('ToDoController@edit', [$group->id, $todo->id]) }}" class="btn btn-default btn-xs">Edit</a>
                                <input type="submit" class="btn btn-default btn-xs" value="Delete">
                            {{-- </span> --}}
                        {{ Form::close() }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

@endsection